resource "aws_cloudformation_stack" "keycloak-stack" {
  name = "keycloak-stack"
  parameters = {
    //      VpcId = module.network.aws_vpc_id
    CertificateArn = "arn:aws:acm:us-east-1:688971144149:certificate/1b6ab546-3aee-46f1-8463-c42602b62c01"
    JavaOpts       = " "
  }
  capabilities  = ["CAPABILITY_IAM"]
  template_body = file("./templates/cloudformation/keycloak-from-new-vpc.json")
}
