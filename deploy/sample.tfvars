POSTGRESQL_PASSWORD        = "administr4tor"
DB_VENDOR                  = "postgres"
POSTGRESQL_DATABASE        = "keycloak"
POSTGRESQL_USERNAME        = "administr4tor"
DB_SCHEMA                  = "public"
KEYCLOAK_USER              = "administr4tor"
KEYCLOAK_PASSWORD          = "administr4tor"
KEYCLOAK_STATISTICS        = "all"
JDBC_PARAMS                = "ssl=false"
JGOURPS_DISCOVERY_PROTOCOL = "JDBC_PING"
