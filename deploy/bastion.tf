
#amazon EC2 image used#
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}


resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  # subnet_id            = aws_subnet.us-east-1b-public.id
  user_data = templatefile("./templates/bastion/user-data.sh", {
    DB_ADDR = aws_db_instance.main.endpoint
    DB_PASSWORD = aws_db_instance.main.password
    DB_USER = aws_db_instance.main.username
    POSTGRESQL_DATABASE = var.POSTGRESQL_DATABASE
  })
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  subnet_id            = module.network.aws_subnet_public_a_id
  key_name             = var.bastion_key_name

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}


resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = module.network.aws_vpc_id

  //  ingress {
  //    protocol    = "tcp"
  //    from_port   = 22
  //    to_port     = 22
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //
  //  egress {
  //    protocol    = "tcp"
  //    from_port   = 443
  //    to_port     = 443
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //
  //  egress {
  //    protocol    = "tcp"
  //    from_port   = 80
  //    to_port     = 80
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }

  ingress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  //  egress {
  //    from_port = 5432
  //    to_port   = 5432
  //    protocol  = "tcp"
  //    cidr_blocks = [
  //      module.network.aws_subnet_private_a_cidr_block,
  //      module.network.aws_subnet_private_b_cidr_block,
  //    ]
  //  }

  tags = local.common_tags
}