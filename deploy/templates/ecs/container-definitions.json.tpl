[
    {
        "name": "keycloak",
        "image": "${keycloak_image}",
        "essential": true,
        "memoryReservation": 1024,
        "environment": [
            {"name": "DB_VENDOR", "value": "${DB_VENDOR}"},
            {"name": "DB_ADDR", "value": "${DB_ADDR}"},
            {"name": "POSTGRESQL_DATABASE", "value": "${POSTGRESQL_DATABASE}"},
            {"name": "DB_USER", "value": "${DB_USER}"},
            {"name": "DB_PASSWORD", "value": "${DB_PASSWORD}"},

            {"name": "KEYCLOAK_USER", "value": "${KEYCLOAK_USER}"},
            {"name": "KEYCLOAK_PASSWORD", "value": "${KEYCLOAK_PASSWORD}"},
            {"name": "KEYCLOAK_STATISTICS", "value": "${KEYCLOAK_STATISTICS}"},
            {"name": "JDBC_PARAMS", "value": "${JDBC_PARAMS}"},
            {"name": "JGOURPS_DISCOVERY_PROTOCOL", "value": "${JDBC_PARAMS}"},

            {"name": "ALLOWED_HOSTS", "value": "${allowed_hosts}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "keycloak"
            }
        },
        "portMappings": [
            {
                "containerPort": 8080,
                "hostPort": 8080
            }
        ],
        "mountPoints": [
            {
                "readOnly": false,
                "containerPath": "/vol/web",
                "sourceVolume": "local"
            }
        ]
    }
]
