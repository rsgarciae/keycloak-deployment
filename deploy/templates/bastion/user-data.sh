#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker centos
sudo yum install postgresql-server -y
sudo service postgresql initdb

PGPASSWORD=$DB_PASSWORD psql -U $DB_USER -d $POSTGRESQL_DATABASE -p 5432 -h $DB_ADDR


