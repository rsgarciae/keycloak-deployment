//#Create One Cluster per environment #
resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

resource "aws_iam_role" "keycloak_iam_role" {
  name               = "${local.prefix}-keycloak-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-keycloak"

  tags = local.common_tags
}


data "template_file" "keycloak_container_definitions" {
  template = file("templates/ecs/container-definitions.json.tpl")

  vars = {
    keycloak_image      = var.ecr_image_keycloak
    DB_ADDR             = aws_db_instance.main.address
    POSTGRESQL_DATABASE = aws_db_instance.main.name
    DB_USER             = aws_db_instance.main.username
    DB_PASSWORD         = aws_db_instance.main.password
    DB_VENDOR           = var.DB_VENDOR

    DB_SCHEMA                  = var.DB_SCHEMA
    KEYCLOAK_USER              = var.KEYCLOAK_USER
    KEYCLOAK_PASSWORD          = var.KEYCLOAK_PASSWORD
    KEYCLOAK_STATISTICS        = var.KEYCLOAK_STATISTICS
    JDBC_PARAMS                = var.JDBC_PARAMS
    JGOURPS_DISCOVERY_PROTOCOL = var.JGOURPS_DISCOVERY_PROTOCOL

    log_group_name   = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region = data.aws_region.current.name
    allowed_hosts    = aws_route53_record.keycloak.fqdn

  }
}

resource "aws_ecs_task_definition" "keycloak" {
  family                   = "${local.prefix}-keycloak"
  container_definitions    = data.template_file.keycloak_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 512
  memory                   = 1024
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.keycloak_iam_role.arn
  volume {
    name = "local"
  }

  tags = local.common_tags
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = module.network.aws_vpc_id

  //  egress {
  //    from_port   = 443
  //    to_port     = 443
  //    protocol    = "tcp"
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      module.network.aws_subnet_private_a_cidr_block,
      module.network.aws_subnet_private_b_cidr_block
    ]
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }
  ingress {
    from_port = 9990
    to_port   = 9990
    protocol  = "tcp"
    security_groups = [
      aws_security_group.lb.id
    ]
  }
  ingress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }
  tags = local.common_tags
}


resource "aws_ecs_service" "keycloak" {
  name            = "${local.prefix}-keycloak"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.keycloak.family
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [
      module.network.aws_subnet_private_a_id,
      module.network.aws_subnet_private_b_id,
      //      module.network.aws_subnet_private_a_id,
      //      module.network.aws_subnet_private_c_id,
      #aws_subnet.public_a.id,
      #aws_subnet.public_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
    #assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.keycloak.arn
    container_name   = "keycloak"
    container_port   = 8080
  }
  depends_on = [aws_lb_listener.keycloak_https]

}
