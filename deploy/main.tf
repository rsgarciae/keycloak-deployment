terraform {
  backend "s3" {
    ##Modify this value with your bucket name for versioning
    bucket = "alpha.keycloak-tfstate"
    ##Modify this key for your project appending .tf-state at the end
    key = "keycloak.tfstate"
    ##modify this value with the region used (us-east-1 as default)
    region  = "us-east-1"
    encrypt = true
    ##Modify this value with the name of the dynamoDB
    dynamodb_table = "keycloak-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
data "aws_region" "current" {}
