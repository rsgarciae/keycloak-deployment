output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_route53_record.keycloak.fqdn
}

output "db_host" {
  value = aws_db_instance.main.address
}

output "load_balancer_ip" {
  value = aws_lb.keycloak.dns_name
}

