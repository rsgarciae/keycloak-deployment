resource "aws_lb" "keycloak" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [
    module.network.aws_subnet_public_a_id,
    module.network.aws_subnet_public_b_id
  ]

  security_groups = [aws_security_group.lb.id]
  tags            = local.common_tags
}

//resource "aws_lb_target_group" "key" {
//  name        = "${local.prefix}-key"
//  protocol    = "HTTP"
//  vpc_id      = module.network.aws_vpc_id
//  target_type = "ip"
//  port        = 9990
//  health_check {
//    path = "/health"
//  }
//}
resource "aws_lb_target_group" "keycloak" {
  name        = "${local.prefix}-keycloak"
  protocol    = "HTTP"
  vpc_id      = module.network.aws_vpc_id
  target_type = "ip"
  port        = 80
  health_check {
    path = "/auth"
  }
}

##this is the entrypoint of the keycloak
resource "aws_lb_listener" "keycloak" {
  load_balancer_arn = aws_lb.keycloak.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "keycloak_https" {
  load_balancer_arn = aws_lb.keycloak.arn
  //  port              = 80
  port = 443
  //  protocol          = "HTTP"
  protocol        = "HTTPS"
  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.keycloak.arn
  }

}


resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = module.network.aws_vpc_id

  //  ingress {
  //    protocol    = "tcp"
  //    from_port   = 80
  //    to_port     = 80
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //
  //  ingress {
  //    protocol    = "tcp"
  //    from_port   = 443
  //    to_port     = 443
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //
  //  egress {
  //    protocol    = "tcp"
  //    from_port   = 8080
  //    to_port     = 8080
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }
  //  egress {
  //    protocol    = "tcp"
  //    from_port   = 9990
  //    to_port     = 9990
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }

  ingress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]

  }


  tags = local.common_tags
}
