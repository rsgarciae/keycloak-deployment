variable "prefix" {
  #Modify this value with the bastion instance name (short proyect name)
  default = "keycloak"
}
##Modify this variable with the name of the proyect
variable "project" {
  default = "keycloak"
}
##Modify this value with the maintainer email
variable "contact" {
  default = "facioo@gmail.com"
}

variable "POSTGRESQL_USERNAME" {
  description = "Username for the RDS Postgres instance"
}

variable "DB_VENDOR" {
  description = "db vendor"
}
variable "POSTGRESQL_DATABASE" {
  description = "database name"
}
variable "DB_SCHEMA" {
  description = "database schema "
}
variable "POSTGRESQL_PASSWORD" {
  description = "database password"
}
variable "KEYCLOAK_USER" {
  description = "keycloak user"
}

variable "KEYCLOAK_PASSWORD" {
  description = "keycloak password"
}
variable "KEYCLOAK_STATISTICS" {
  description = "keycloak statistics"
}

variable "JDBC_PARAMS" {
  description = "keycloak ssh status"
}

variable "JGOURPS_DISCOVERY_PROTOCOL" {
  description = "keycloak ssh status"
}

variable "bastion_key_name" {
  default = "keycloak-bastion"
}

variable "ecr_image_keycloak" {
  description = "ECR Image for API"
  default     = "688971144149.dkr.ecr.us-east-1.amazonaws.com/keycloak:latest"
}


variable "dns_zone_name" {
  description = "Domain name"
  default     = "myuse-commerce.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "keycloak"
    staging    = "keycloak.staging"
    dev        = "keycloak.dev"
  }
}