FROM alphacs/keycloak:jboss.1.0
# FROM  quay.io/keycloak/keycloak:15.0.2
LABEL maintainer="Alpha Consulting Systems"

VOLUME /local


EXPOSE 8080
